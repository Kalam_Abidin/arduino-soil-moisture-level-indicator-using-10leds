const int sensorPin = A0;             // Pin connected to the sensor's analog output
const int ledPins[10] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11}; // Array of LED pins
int sensorValue = 0;                  // Variable to store sensor value
int moistureLevel = 0;                // Variable to store mapped moisture level

void setup() {
  Serial.begin(9600);                 // Initialize serial communication for debugging
  for (int i = 0; i < 10; i++) {
    pinMode(ledPins[i], OUTPUT);      // Set each LED pin as output
    digitalWrite(ledPins[i], LOW);    // Ensure all LEDs start in the off state
  }
}

void loop() {
  sensorValue = analogRead(sensorPin);  // Read value from soil moisture sensor

  // Adjust the mapping to better match the sensor's dry and wet conditions
  moistureLevel = map(sensorValue, 0, 895, 0, 10); // Map sensor value to moisture level (0-10)

  // Ensure the moisture level is within the bounds of 0 to 10
  moistureLevel = constrain(moistureLevel, 0, 10);

  // Print sensor value and moisture level for debugging
  Serial.print("Sensor Value: ");
  Serial.print(sensorValue);
  Serial.print(" - Moisture Level: ");
  Serial.println(moistureLevel);

  // Light up LEDs according to the moisture level
  for (int i = 0; i < 10; i++) {
    if (i < moistureLevel) {
      Serial.print("LED ");
      Serial.print(ledPins[i]);
      Serial.println(" ON");
      digitalWrite(ledPins[i], HIGH); // Turn on LED if i is less than moisture level
    } else {
      Serial.print("LED ");
      Serial.print(ledPins[i]);
      Serial.println(" OFF");
      digitalWrite(ledPins[i], LOW);  // Turn off LED if i is greater or equal to moisture level
    }
  }

  delay(200); // Small delay to avoid flickering
}
